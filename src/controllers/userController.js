import { getSysMsUser, insertSysMsUser } from "../services/sysMsUserService"

const index = async (req, res) => {
  try {
    getSysMsUser((err, data) => {
      if (err) {
        res.status(400).send(err)
      }

      res.status(200).send(data)
    })
  } catch (e) {
    res.status(400).send(e)
  }
}

const save = async (req, res) => {
  try {
    insertSysMsUser(req.body, (err, data) => {
      if (err) {
        res.status(400).send(err)
      }

      res.status(200).send(data)
    })
  } catch (e) {
    res.status(400).send(e)
  }
}

export {
  index,
  save,
}
