import { getAuthUrl, getInfoOnCallback, getInfo } from "../services/googleApiService";

const index = async (req, res) => {
  getAuthUrl((err, url) => {
    if(err) {
      res.status(400).send(err)
    }
    
    res.status(200)
    // .send(url)
    .set('Content-Type', 'text/html')
    .send(new Buffer.from(`<a href=${url}>Login</a>`))
  });
}

const callback = async (req, res) => {
  console.log(req.query.code)
  getInfoOnCallback(req.query.code, (err, userinfo) => {
    if(err) {
      res.status(400).send(err)
    }

    res.status(200).send(userinfo)
  })
}

const info = async (req, res) => {
  getInfo("", (err, userinfo) => {
    if(err) {
      res.status(400).send(err)
    }

    res.status(200).send(userinfo)
  })
}

export {
  index,
  callback,
  info
}
