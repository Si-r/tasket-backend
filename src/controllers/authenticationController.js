import { loginService, logoutService } from "../services/authenticationService"

const index = async (req, res) => {
  res.status(200).send("")
}

const login = async (req, res) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    loginService(email, password, (err, data) => {
      if (err) {
        res.status(200).send({
          error : {
            code: 404,
            message: err
          }
        })
      }

      res.status(200).send(data)
    })
  }catch (e) {
    console.log(e);
    
    res.status(400).send({
      error : {
        code: 404,
        message: e
      }
    })
  }
}

const logout = async (req, res) => {
  try {
    insertSysMsUser(req.body, (err, data) => {
      if (err) {
        res.status(400).send(err)
      }

      res.status(200).send(data)
    })
  } catch (e) {
    res.status(400).send(e)
  }
}

export {
  login,
  logout,
}
