import { verify } from 'jsonwebtoken'
import sysMsUserModel from '../models/sysMsUser.model'

const verifyToken = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer:', '')
    const decoded = verify(token, process.env.JWT_SECRET)
    const user = await sysMsUserModel.findOne({ _id: decoded._id})

    if (!user) {
      throw new Error("token expired")
    }

    req.token = token
    req.user = user
    next()
  } catch (e) {
    console.log(e);
    res.status(401).send({ error: 'Please authenticate.' })
  }
}

export default verifyToken