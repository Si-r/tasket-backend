import sysMsUser from '../models/sysMsUser.model.js'
import message from "../config/message.schema"
import { format } from "util"

const loginService = async (email, password, callback) => {
  if (email === undefined) {
    return callback(format(message.main.required, "email"))
  }

  if (password  === undefined) {
    return callback(format(message.main.required, "password"))
  }
  try {
    const user = await sysMsUser.findByCredentials(email, password)
    const token = await user.generateAuthToken()

    callback(null, { user, token })
  } catch (error) {
    console.log(error);
    
    callback(error.message)
  }
}

const logoutService = (callback) => {
  callback(null, "")
}

export {
  loginService,
  logoutService
}