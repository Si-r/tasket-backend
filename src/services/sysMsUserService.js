import sysMsUser from '../models/sysMsUser.model.js'

const getSysMsUser = (callback) => {
  sysMsUser.find({}, (err, sysMsUser) => {
    if (err) {
      res.send(err)
    }

    callback(null, sysMsUser)
  });
}

const insertSysMsUser = (data, callback) => {
  data = {
    email: data.email,
    username: data.username,
    password: data.password,
    isActive: "false",
    createdBy: "admin",
  }

  const newSysMsUser = new sysMsUser(data);
  newSysMsUser.save((err, sysMsUser) => {
    if (err) {
      return callback(err)
    }

    callback(null, sysMsUser)
  })
}

const updateSysMsUser = (req, callback) => {
  sysMsUser.findOneAndUpdate({
    _id: req.params.noteId
  }, req.body,
    (err, sysMsUser) => {
      if (err) {
        callback(err)
      }

      callback(null, sysMsUser)
    }
  );
}

const deleteSysMsUser = (req, callback) => {
  note.remove({
    _id: req.params.sysMsUserId
  }, (err) => {
    if (err) {
      callback(err);
    }

    callback(
      null,
      {
        message: `user ${req.params.sysMsUserId} successfully deleted`
      }
    )
  })
}

export {
  getSysMsUser,
  insertSysMsUser,
  updateSysMsUser,
  deleteSysMsUser
}