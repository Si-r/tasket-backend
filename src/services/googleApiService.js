import { google } from 'googleapis';
const ClientId = "258773099904-hdki8qglhdeq8if0c3lghdj49pki8vf5.apps.googleusercontent.com";
const ClientSecret = "n7ZXRCcZkD4S5OZXKl1CCRZN";
const RedirectionUrl = "http://localhost:3000/oauthCallback";
// const RedirectionUrl = "http://www.krapao-tech.com/oauthCallback";

const oauth2Client = new google.auth.OAuth2(ClientId, ClientSecret, RedirectionUrl)
const plus = (auth) => {
  return google.plus({ version: 'v1', auth });
}

// const getGooglePlusApi = (auth) => {
//   return google.plus({ version: 'v1', auth:auth });
// }

const getAuthUrl = async (callback) => {
  try {
    // generate a url that asks permissions for Google+ and Google Calendar scopes
    const scopes = [
      'https://www.googleapis.com/auth/plus.me',
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ];

    const url = oauth2Client.generateAuthUrl({
      access_type: 'online',
      prompt: 'consent',
      scope: scopes // If you only need one scope you can pass it as string
    });

    callback(null, url);
  } catch (error) {
    // console.log(error);
    callback(error.message)
  }
}

const getInfoOnCallback = async (code, callback) => {
  try {
    const {tokens} = await oauth2Client.getToken(code);
    oauth2Client.credentials = tokens;

    console.log(tokens)

    const me = await plus(oauth2Client).people.get({ userId: 'me'});
    const userGoogleId = me.data.id;
    const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;
    
    // return so we can login or sign up the user
    
    callback(null, me.data)
  } catch (err) {
    console.log(err)
    callback(err)
  }
}

const getInfo = async (tokens, callback) => {
  try {
    oauth2Client.credentials = { access_token:
      'ya29.Il-RB9RVPYtWWBtdM2SPTI29uKOI9cZKBjMRf_SRVrybsTUTNPtoa2sLZ5zt45NCn60JWlBBZlocJzBtf43NtJYYFArlaGbA-S2wmrw2CJBNhCWqcEazZsE-H7hXP1pweA',
     scope:
      'openid https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email',
     token_type: 'Bearer',
     id_token:
      'eyJhbGciOiJSUzI1NiIsImtpZCI6IjhjNThlMTM4NjE0YmQ1ODc0MjE3MmJkNTA4MGQxOTdkMmIyZGQyZjMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIyNTg3NzMwOTk5MDQtaGRraThxZ2xoZGVxOGlmMGMzbGdoZGo0OXBraTh2ZjUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyNTg3NzMwOTk5MDQtaGRraThxZ2xoZGVxOGlmMGMzbGdoZGo0OXBraTh2ZjUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTQ3NDE4MTczMDQ5Mzc2NTU1OTUiLCJlbWFpbCI6InJld29ybGR0QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiX2VsNmcwT2JtMmZmUndDV0N4aTJtUSIsIm5hbWUiOiLguKrguLTguJfguJjguLIg4Lij4Lix4LiB4Lip4Liy4Liq4Lin4Lix4Liq4LiU4Li04LmMIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BQXVFN21BMy1qOHZNME9QcTRna3VYTmpxQjNuSEd5UWFsblBpTkZyMDJPMjh3PXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IuC4quC4tOC4l-C4mOC4siIsImZhbWlseV9uYW1lIjoi4Lij4Lix4LiB4Lip4Liy4Liq4Lin4Lix4Liq4LiU4Li04LmMIiwibG9jYWxlIjoidGgiLCJpYXQiOjE1Njk3NzY3MTIsImV4cCI6MTU2OTc4MDMxMn0.CAOgNo3JZbhYN3HWKA-YlJ10BXXrZgT7GuG2LQHMUjiNlaUiV2VzRioVOhvCeV6ZnYeZ-VhBuHzED3WUvtXlIgjpWn-0l5eU1pt-uzsZOSspvlnC1VgS1nkpTY_H-KIcbnF9KyOfqEBveJzu7SbY3rFWM3_2vFrTOUd5PEpsPgk2k-ThJABw5Rnc3MoQaMNBUlKh1WSxVOnN3NvyImRJqTR9MUQhegGh01pQq16DbSuoVIltnPy2csUOotLrP_bqrPTYVTQwaMbEnMMsT4Olqb9MfYDTsxvwT6fwUoPLOugzDeB806q1EV7gustIlkUGm759plYiGGei5EQm3LnwWw',
     expiry_date: 1569780311181 };

    console.log(tokens)

    const me = await plus(oauth2Client).people.get({ userId: 'me'});
    const userGoogleId = me.data.id;
    const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;
    
    // return so we can login or sign up the user
    
    callback(null, me.data)
  } catch (err) {
    console.log(err)
    callback(err)
  }
}

export {
  getAuthUrl,
  getInfoOnCallback,
  getInfo
} 
