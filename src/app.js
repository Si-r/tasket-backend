import express from 'express'
import './db/mongoose'
import routes from './routers/index'

const app = express()

app.use(express.json())

Object.keys(routes).map(
    (k) => app.use(routes[k])
)

export default app