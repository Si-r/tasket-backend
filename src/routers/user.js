import { Router } from "express"
import { index, save } from "../controllers/userController"
import verifyToken from "../middleware/VerifyToken"

const router = new Router()

router
  .route("/sysMsUser")
  .get(verifyToken, index)
  .post(save)

export default router
