import { Router } from "express";
import user from "./user";
import authentication from "./authentication"
import { index, callback, info } from "../controllers/welcomeController";
const router = new Router()

router.route("/").get(index)

router.route("/oauthCallback").get(callback)

router.route("/info").get(info)


export default {
  router,
  user,
  authentication
}
