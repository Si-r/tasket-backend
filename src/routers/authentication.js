import { Router } from "express"
import { login } from "../controllers/authenticationController"

const router = new Router()

router
  .route("/authentication/login")
  .post(login)

export default router
