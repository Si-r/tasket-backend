import { Schema, model } from "mongoose"
import { format } from "util"
import bcrypt from "bcryptjs"
import auditModel from "./audit.model"
import message from "../config/message.schema"
import jwt from "jsonwebtoken"
import tokensHasUserModel from "./tokensHasUser.model"

class SysMsUserModel extends auditModel {
  constructor() {
    super()
    this.email = {
      type: String,
      unique: true,
      required: [true, format(message.main.required, "Email")],
      trim: true
    }

    this.username = {
      type: String,
      unique: true,
      required: [
        true,
        format(message.main.required, "Username")
      ],
      trim: true
    }

    this.password = {
      type: String,
      required: [
        true,
        format(message.main.required, "Password")
      ],
      trim: true
    }

    this.isActive = {
      type: Boolean,
      default: new Boolean(false)
    }

    this.isOnline = {
      type: Boolean,
      default: new Boolean(false)
    }
  }
}

const sysMsUser = new Schema(new SysMsUserModel())

sysMsUser.virtual('tokens_has_user', {
  ref: 'tokens_has_user',
  localField: '_id',
  foreignField: 'owner'
})

sysMsUser.set('timestamps', true)

sysMsUser.methods.toJSON = function () {
  const user = this
  const userObject = user.toObject()

  delete userObject.password
  delete userObject.tokens
  delete userObject.avatar
  delete userObject.createdBy
  delete userObject.updatedBy

  return userObject
}

sysMsUser.statics.findByCredentials = async (email, password) => {
  const user = await sysMsUserModel.findOne({ email })

  if (!user) {
      throw new Error(message.sysMsUser.emailNotFound)
  }

  const isMatch = await bcrypt.compare(password, user.password)

  if (!isMatch) {
      throw new Error(message.sysMsUser.loginFail)
  }

  user.isOnline = true
  user.update();

  return user
}

sysMsUser.methods.generateAuthToken = async function () {
  const user = this

  const options = {
    _id: user._id.toString(),
    username: user.username,
  }

  const token = jwt.sign(options, process.env.JWT_SECRET)

  const newToken = {
    "token": token,
    isActive: true,
    owner: user._id.toString()
  }

  const tokensHasUser = tokensHasUserModel(newToken)
  await tokensHasUser.save()

  return token
}


sysMsUser.pre('save', async function (next) {
  const user = this

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8)
  }

  next()
})

sysMsUser.pre('update', async function (next) {
  const user = this

  next()
})

const sysMsUserModel = model("sys_ms_user", sysMsUser)

export default sysMsUserModel
