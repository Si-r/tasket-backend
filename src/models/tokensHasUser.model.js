import { Schema, model, mongoose } from "mongoose"
import { format } from "util"
import bcrypt from "bcryptjs"
import auditModel from "./audit.model"
import message from "../config/message.schema"
import jwt from "jsonwebtoken"


class tokensModel extends auditModel {
  constructor() {
    super()

    this.token = {
      type: String,
      required: [
        true,
        format(message.main.required, "token")
      ]
    }

    this.isActive = {
      type: Boolean,
      default: new Boolean(false)
    }

    this.owner = {
      type: Schema.Types.ObjectId,
      required: [
        true,
        format(message.main.required, "sysMsUser._Id")
      ],
      ref: 'sys_ms_user'
    }

    this.timestamps
  }
}

const tokensHasUser = new Schema(new tokensModel())

tokensHasUser.set('timestamps', true)

const tokensHasUserModel = model("tokens_has_user", tokensHasUser)
export default tokensHasUserModel