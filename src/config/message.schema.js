const message = {
    main : {
      required : "Please enter %s",
    },
    sysMsUser : {
        emailNotFound : "E-Mail Not Found",
        loginFail: "Unable to login"
    }
}

export default message